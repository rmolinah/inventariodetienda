/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventariodetienda;

import static inventariodetienda.Main.mListaDeUsuarios;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Rose Molina
 */

/*
 * La clase frmCrearRoles hereda de la clase JFrame que se
 * encuentra en la libreria javax y paquete swing
 */
public class FrmResultadosGlobalesDeEncuestas extends javax.swing.JFrame {

    String[] mNombreColumnas = {"Pregunta", "Totalmente en desacuerdo", "En desacuerdo", "Neutral", "De acuerdo", "Totalmente de acuerdo"};
    int contadorp1v1 = 0;
    int contadorp1v2 = 0;
    int contadorp1v3 = 0;
    int contadorp1v4 = 0;
    int contadorp1v5 = 0;

    int contadorp2v1 = 0;
    int contadorp2v2 = 0;
    int contadorp2v3 = 0;
    int contadorp2v4 = 0;
    int contadorp2v5 = 0;

    int contadorp3v1 = 0;
    int contadorp3v2 = 0;
    int contadorp3v3 = 0;
    int contadorp3v4 = 0;
    int contadorp3v5 = 0;

    public FrmResultadosGlobalesDeEncuestas() {
        this.initComponents();

        //Llenar tabla de experiencias
        String nombreEncuesta = "Experiencia del aplicativo";
        llenarConResultadosTabla(nombreEncuesta);
        JFreeChart grafico = cargarGraficoResultadoEncuesta(nombreEncuesta);        
        
        ChartPanel panelGrafico = new ChartPanel(grafico);
        pnlGrafico1.removeAll();
        pnlGrafico1.add(panelGrafico, BorderLayout.CENTER);
        pnlGrafico1.validate();

        //LLenar tabla personal
        nombreEncuesta = "Personal";
        llenarConResultadosTabla(nombreEncuesta);
        grafico= cargarGraficoResultadoEncuesta(nombreEncuesta);
        
        panelGrafico = new ChartPanel(grafico);
        pnlGrafico2.removeAll();
        pnlGrafico2.add(panelGrafico, BorderLayout.CENTER);
        pnlGrafico2.validate();
    }

    public void llenarConResultadosTabla(String pNombreEncuesta) {

        contadorp1v1 = 0;
        contadorp1v2 = 0;
        contadorp1v3 = 0;
        contadorp1v4 = 0;
        contadorp1v5 = 0;

        contadorp2v1 = 0;
        contadorp2v2 = 0;
        contadorp2v3 = 0;
        contadorp2v4 = 0;
        contadorp2v5 = 0;

        contadorp3v1 = 0;
        contadorp3v2 = 0;
        contadorp3v3 = 0;
        contadorp3v4 = 0;
        contadorp3v5 = 0;

        String mPregunta1 = "";
        String mPregunta2 = "";
        String mPregunta3 = "";

        DefaultTableModel modeloTabla = new DefaultTableModel(mNombreColumnas, 0);

        Iterator<Usuario> iteradorUsuario = mListaDeUsuarios.iterator();

        while (iteradorUsuario.hasNext()) {

            Usuario tmpUsuario = iteradorUsuario.next();
            boolean hizoEncuesta = false;

            if (pNombreEncuesta.contains("Experiencia")) {
                hizoEncuesta = tmpUsuario.getHizoEncuestaExperiencia();

            } else if (pNombreEncuesta.contains("Personal")) {
                hizoEncuesta = tmpUsuario.getHizoEncuestaPersonal();
            }
            if (hizoEncuesta) {

                ArrayList<Encuesta> listaEncuestas = tmpUsuario.getListaEncuestas();

                Iterator<Encuesta> iteradorEncuesta = listaEncuestas.iterator();

                while (iteradorEncuesta.hasNext()) {

                    Encuesta encuesta = iteradorEncuesta.next();

                    if (encuesta.getNombre().equals(pNombreEncuesta)) {

                        mPregunta1 = encuesta.getPregunta1();
                        mPregunta2 = encuesta.getPregunta2();
                        mPregunta3 = encuesta.getPregunta3();

                        //Pregunta1
                        switch (encuesta.getRespuesta1()) {
                            case 1:
                                contadorp1v1++;
                                break;
                            case 2:
                                contadorp1v2++;
                                break;
                            case 3:
                                contadorp1v3++;
                                break;
                            case 4:
                                contadorp1v4++;
                                break;
                            case 5:
                                contadorp1v5++;
                                break;
                            default:
                                break;
                        }

                        //Pregunta 2
                        switch (encuesta.getRespuesta2()) {
                            case 1:
                                contadorp2v1++;
                                break;
                            case 2:
                                contadorp2v2++;
                                break;
                            case 3:
                                contadorp2v3++;
                                break;
                            case 4:
                                contadorp2v4++;
                                break;
                            case 5:
                                contadorp2v5++;
                                break;
                            default:
                                break;
                        }

                        //Pregunta 3
                        switch (encuesta.getRespuesta3()) {
                            case 1:
                                contadorp3v1++;
                                break;
                            case 2:
                                contadorp3v2++;
                                break;
                            case 3:
                                contadorp3v3++;
                                break;
                            case 4:
                                contadorp3v4++;
                                break;
                            case 5:
                                contadorp3v5++;
                                break;
                            default:
                                break;
                        }

                    }
                }

            }

        }

        //Llenar las filas con los resultados
        Object[] fila1 = {mPregunta1, contadorp1v1, contadorp1v2, contadorp1v3, contadorp1v4, contadorp1v5};
        Object[] fila2 = {mPregunta2, contadorp2v1, contadorp2v2, contadorp2v3, contadorp2v4, contadorp2v5};
        Object[] fila3 = {mPregunta3, contadorp3v1, contadorp3v2, contadorp3v3, contadorp3v4, contadorp3v5};
        Object[] fila4 = {"Sumatoria de resultados", (contadorp1v1 + contadorp2v1 + contadorp3v1), (contadorp1v2 + contadorp2v2 + contadorp3v2), (contadorp1v3 + contadorp2v3 + contadorp3v3), (contadorp1v4 + contadorp2v4 + contadorp3v4), (contadorp1v5 + contadorp2v5 + contadorp3v5)};

        modeloTabla.addRow(fila1);
        modeloTabla.addRow(fila2);
        modeloTabla.addRow(fila3);
        modeloTabla.addRow(fila4);

        //Llenar la tabla correspondiente con el modelo
        if (pNombreEncuesta.contains("Experiencia")) {

            tablaExperiencia.setModel(modeloTabla);
            //Personalizar el tamaño(ancho ) de las columnas
            int[] tamañoColumnas = {230, 175, 100, 100, 150, 150};

            int i = 0;

            for (int ancho : tamañoColumnas) {
                TableColumn columna = tablaExperiencia.getColumnModel().getColumn(i++);
                columna.setMinWidth(ancho);
                columna.setMaxWidth(ancho);
                columna.setPreferredWidth(ancho);
            }

            //Establecer el alto de una tabla
            tablaExperiencia.setRowHeight(20);

        } else if (pNombreEncuesta.contains("Personal")) {
            tablaPersonal.setModel(modeloTabla);

            //Personalizar el tamaño(ancho ) de las columnas
            int[] tamañoColumnas = {500, 175, 100, 100, 150, 150};

            int i = 0;

            for (int ancho : tamañoColumnas) {
                TableColumn columna = tablaPersonal.getColumnModel().getColumn(i++);
                columna.setMinWidth(ancho);
                columna.setMaxWidth(ancho);
                columna.setPreferredWidth(ancho);
            }

            //Establecer el alto de una tabla
            tablaPersonal.setRowHeight(20);

        }

    }

    public JFreeChart cargarGraficoResultadoEncuesta(String pNombreEncuesta) {

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        dataset.setValue((contadorp1v1 + contadorp2v1 + contadorp3v1), "", "Totalmente en desacuerdo");
        dataset.setValue((contadorp1v2 + contadorp2v2 + contadorp3v2), "", "En desacuerdo");
        dataset.setValue((contadorp1v3 + contadorp2v3 + contadorp3v3), "", "Neutral");
        dataset.setValue((contadorp1v4 + contadorp2v4 + contadorp3v4), "", "De acuerdo");
        dataset.setValue((contadorp1v5 + contadorp2v5 + contadorp3v5), "", "Totalmente de acuerdo");

        JFreeChart grafico = ChartFactory.createBarChart("Grafico " + pNombreEncuesta + " general", "", "", dataset, PlotOrientation.HORIZONTAL, false, false, false);
        CategoryPlot catPlot = grafico.getCategoryPlot();
        catPlot.setRangeGridlinePaint(Color.black);
        
        return grafico;

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBlanco = new javax.swing.JPanel();
        pnlAqua = new javax.swing.JPanel();
        lblBienvenido = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaExperiencia = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaPersonal = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        TablaPersonal = new javax.swing.JLabel();
        pnlGrafico1 = new javax.swing.JPanel();
        pnlGrafico2 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Creacion de Usuarios");
        setName("Inicio de sesion"); // NOI18N

        pnlBlanco.setBackground(new java.awt.Color(255, 255, 255));

        pnlAqua.setBackground(new java.awt.Color(22, 144, 163));

        javax.swing.GroupLayout pnlAquaLayout = new javax.swing.GroupLayout(pnlAqua);
        pnlAqua.setLayout(pnlAquaLayout);
        pnlAquaLayout.setHorizontalGroup(
            pnlAquaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 82, Short.MAX_VALUE)
        );
        pnlAquaLayout.setVerticalGroup(
            pnlAquaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        lblBienvenido.setFont(new java.awt.Font("Perpetua", 0, 48)); // NOI18N
        lblBienvenido.setText("Resultados globales de encuestas");

        tablaExperiencia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tablaExperiencia);

        tablaPersonal.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tablaPersonal);

        jLabel1.setFont(new java.awt.Font("Tekton Pro", 0, 24)); // NOI18N
        jLabel1.setText("Experiencia");

        TablaPersonal.setFont(new java.awt.Font("Tekton Pro", 0, 24)); // NOI18N
        TablaPersonal.setText("Personales");

        pnlGrafico1.setBackground(new java.awt.Color(204, 204, 255));
        pnlGrafico1.setLayout(new java.awt.BorderLayout());

        pnlGrafico2.setBackground(new java.awt.Color(204, 204, 255));
        pnlGrafico2.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout pnlBlancoLayout = new javax.swing.GroupLayout(pnlBlanco);
        pnlBlanco.setLayout(pnlBlancoLayout);
        pnlBlancoLayout.setHorizontalGroup(
            pnlBlancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBlancoLayout.createSequentialGroup()
                .addComponent(pnlAqua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(pnlBlancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlBlancoLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(pnlBlancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(TablaPersonal)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlBlancoLayout.createSequentialGroup()
                                .addComponent(pnlGrafico1, javax.swing.GroupLayout.PREFERRED_SIZE, 555, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                                .addComponent(pnlGrafico2, javax.swing.GroupLayout.PREFERRED_SIZE, 569, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane2)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 915, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlBlancoLayout.createSequentialGroup()
                        .addGap(264, 264, 264)
                        .addComponent(lblBienvenido)))
                .addContainerGap(59, Short.MAX_VALUE))
        );
        pnlBlancoLayout.setVerticalGroup(
            pnlBlancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBlancoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblBienvenido)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(TablaPersonal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(pnlBlancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlGrafico1, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnlGrafico2, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addComponent(pnlAqua, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBlanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlBlanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmResultadosGlobalesDeEncuestas.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmResultadosGlobalesDeEncuestas.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmResultadosGlobalesDeEncuestas.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmResultadosGlobalesDeEncuestas.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmResultadosGlobalesDeEncuestas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel TablaPersonal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblBienvenido;
    private javax.swing.JPanel pnlAqua;
    private javax.swing.JPanel pnlBlanco;
    private javax.swing.JPanel pnlGrafico1;
    private javax.swing.JPanel pnlGrafico2;
    private javax.swing.JTable tablaExperiencia;
    private javax.swing.JTable tablaPersonal;
    // End of variables declaration//GEN-END:variables
}
